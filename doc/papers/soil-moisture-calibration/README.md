# Papers review

## Using ML

- Carranza, C., Nolet, C., Pezij, M., & van der Ploeg, M. (2021). **Root zone soil moisture estimation with Random Forest**. Journal of Hydrology, 593(December 2020), 125840. https://doi.org/10.1016/j.jhydrol.2020.125840

    In this study, the Random Forest (RF) ensemble learning algorithm, is tested to demonstrate the capabilities and advantages of ML for Root zone soil moisture (RZSM) estimation. Interpolation and extrapolation of RZSM on a daily timescale was carried out using RF. RF predictions have slightly higher accuracy for interpolation and similar accuracy for extrapolation in comparison with RZSM simulated from a process-based model.

- Coopersmith, E. J., Cosh, M. H., Bell, J. E., & Boyles, R. (2016). **Using machine learning to produce near surface soil moisture estimates from deeper in situ records at U.S. Climate Reference Network (USCRN) locations: Analysis and applications to AMSR-E satellite validation**. Advances in Water Resources, 98, 122–131. https://doi.org/10.1016/j.advwatres.2016.10.007

    Soil moisture sensors from the U.S. Climate Reference Net- work (USCRN) were used to generate models of 5 cm soil moisture, with 10 cm soil moisture measure- ments and antecedent precipitation as inputs, via machine learning techniques. Validation was conducted with the available, in situ , 5 cm resources.

- Yamaç, S. S., Şeker, C., & Negiş, H. (2020). **Evaluation of machine learning methods to predict soil moisture constants with different combinations of soil input data for calcareous soils in a semi arid area**. Agricultural Water Management, 234(December 2019). https://doi.org/10.1016/j.agwat.2020.106121


    This study evaluated the performance of deep learning (DL), artificial neural network (ANN) and k-nearest neighbour (kNN) models to estimate field capacity (FC) and permanent wilting point (PWP) using four com-binations of soil data.Therefore, the DL model could be recommended for the estimation of FC when full soil data are available and the kNN model could be recommended for estimation of PWP with all combinations of soil data

- Liu, Y., Jing, W., Wang, Q., & Xia, X. (2020). **Generating high-resolution daily soil moisture by using spatial downscaling techniques: a comparison of six machine learning algorithms**. Advances in Water Resources, 141, 103601. https://doi.org/10.1016/j.advwatres.2020.103601

    In this study, the performance of multiple machine learning algorithms in downscaling the Essential Climate Variable Program initiated by the European Space Agency Soil Moisture dataset was validated over different underlying surfaces.Six machine learning algorithms: artificial neural network (ANN), Bayesian (BAYE), classification and regression trees (CART), K nearest neighbor (KNN), random forest (RF), and support vector machine (SVM), were implemented to establish the spatial downscaling models over four case study areas with reliable continuous in-situ SM observations over four case study areas.

- Chen, L., Zhangzhong, L., Zheng, W., Yu, J., Wang, Z., Wang, L., & Huang, C. (2019). **Data-driven calibration of soil moisture sensor considering impacts of temperature: A case study on FDR sensors**. Sensors (Switzerland), 19(20). https://doi.org/10.3390/s19204381

    In this study, the temperature impact on the soil moisture sensor reading was firstly analyzed. Next, a pioneer study on the data-driven calibration of soil moisture sensor was investigated considering the impacts of temperature.
    To our best knowledge, it is the first time that data-driven methods have been applied into the calibration of soil moisture sensor. Two data-driven models including the multivariate adaptive regression splines (MARS) [15,16] and the Gaussian process regression (GPR) [17,18] were developed to calibrate soil moisture sensor considering the impacts of temperature on the accuracy of measurements.

- Achieng, K. O. (2019). **Modelling of soil moisture retention curve using machine learning techniques: Artificial and deep neural networks vs support vector regression models**. Computers and Geosciences, 133(August), 104320. https://doi.org/10.1016/j.cageo.2019.104320

    In this study, plausibility of various machine learning techniques to simulate soil moisture retention curve of loamy sand are evaluated. Specifically, the machine learning techniques that are investigated include: three support vector regression (SVR) models (i.e. radial basis function (RBF), linear and polynomial kernels), single-layer artificial neural network (ANN), and deep neural network (DNN). The soil moisture and soil suction were measured using time-domain reflectometer (TDR) and tensiometer, respectively.

- Chen, S., She, D., Zhang, L., Guo, M., & Liu, X. (2019). **Spatial downscaling methods of soil moisture based on multisource remote sensing data and its application**. Water (Switzerland), 11(7), 1–25. https://doi.org/10.3390/w11071401

    This study attempted to utilize machine learning and data mining algorithms to downscale the Advanced Microwave Scanning Radiometer-Earth Observing System (AMSR-E) soil moisture data from 25 km to 1 km and compared the advantages and disadvantages of the random forest model and the Cubist algorithm to determine the more suitable soil moisture downscaling method for the middle and lower reaches of the Yangtze River Basin

- Yu, Z., Bedig, A., Montalto, F., & Quigley, M. (2018). **Automated detection of unusual soil moisture probe response patterns with association rule learning**. Environmental Modelling and Software, 105, 257–269. https://doi.org/10.1016/j.envsoft.2018.04.001

    In this paper, we develop a rule-based learning algorithm involving Dynamic Time Warping (DTW) to investigate the feasibility of detecting anomalous responses from soil moisture probes


## Classical approach
- Domínguez-Niño, J. M., Bogena, H. R., Huisman, J. A., Schilling, B., & Casadesús, J. (2019). On the accuracy of factory-calibrated low-cost soil water content sensors. Sensors (Switzerland), 19(14). https://doi.org/10.3390/s19143101
    
    The aim of this paper is to test the degree of improvement of various sensor- and soil-specific calibration options compared to factory calibrations. The second step involved the establishment of a site-specific relationship between permittivity and soil water content using undisturbed soil samples and time domain reflectometry (TDR) measurements

- Zemni, N., Bouksila, F., Persson, M., Slama, F., Berndtsson, R., & Bouhlila, R. (2019). Laboratory calibration and field validation of soil water content and salinity measurements using the 5TE sensor. Sensors (Switzerland), 19(23), 1–18. https://doi.org/10.3390/s19235272

    the objective of the present study was to assess the performance of the 5TE sensor to estimate soil water content and soil pore electrical conductivity for a representative sandy soil used for cultivation of date palms. Both standard models and a novel approach using corrected models to compensate for high electrical conductivity were used

- Ferrarezi, R. S., Nogueira, T. A. R., & Zepeda, S. G. C. (2020). Performance of soil moisture sensors in Florida Sandy Soils. Water (Switzerland), 12(2), 1–20. https://doi.org/10.3390/w12020358

    A laboratory study was conducted to evaluate the performance of several commercial sensors and to establish soil-specific calibration equations for different soil types.Soil-specific calibrations from this study resulted in accuracy expressed as root mean square error (RMSE)

- Zhou, W., Xu, Z., Ross, D., Dignan, J., Fan, Y., Huang, Y., Wang, G., Bagtzoglou, A. C., Lei, Y., & Li, B. (2019). **Towards water-saving irrigation methodology: Field test of soil moisture profiling using flat thin mm-sized soil moisture sensors (MSMSs)**. Sensors and Actuators, B: Chemical, 298(July), 126857. https://doi.org/10.1016/j.snb.2019.126857

    The 10-month field tests conducted at a farm site compared three groups of MSMS with commercial capacitance-type soil moisture sensors (SMS) in terms of accuracy, sensitivity to environmental variations (e.g. water shock, temperatures, dry/ wet seasons) and long-term stability

- Nagahage, E. A. A. D., Nagahage, I. S. P., & Fujino, T. (2019). **Calibration and validation of a low-cost capacitive moisture sensor to integrate the automated soil moisture monitoring system**. Agriculture (Switzerland), 9(7). https://doi.org/10.3390/agriculture9070141

    We have developed a prototype for automated soil moisture monitoring using a low-cost capacitive soil moisture sensor (SKU:SEN0193) for data acquisition, connected to the internet. A soil-specific calibration was performed to integrate the sensor with the automated soil moisture monitoring system. The accuracy of the soil moisture measurements was compared with those of a gravimetric method and a well-established soil moisture sensor (SM-200, Delta-T Devices Ltd, Cambridge, UK).

- Jiménez, A. de los Á. C., Almeida, C. D. G. C. de, Santos Júnior, J. A,Morais, J. E. F. de, Almeida, B. G. de, & Andrade, F. H. N. de. (2019). **Accuracy of capacitive sensors for estimating soil moisture in northeastern Brazil**. Soil and Tillage Research, 195(September), 104413. https://doi.org/10.1016/j.still.2019.104413

    The objective of this study was to calibrate the YL-69 sensor and compare its accuracy with two commercial ECH2O probes.

- Bogena, H. R., Huisman, J. A., Schilling, B., Weuthen, A., & Vereecken, H. (2017). **Effective calibration of low-cost soil water content sensors**. Sensors (Switzerland), 17(1). https://doi.org/10.3390/s17010208

    Here, we present an effective calibration method to improve the measurement accuracy of low-cost soil water content sensors taking the recently developed SMT100 sensor (Truebner GmbH, Neustadt, Germany) as an example. We calibrated the sensor output of more than 700 SMT100 sensors to permittivity using a standard procedure based on five reference media with a known apparent dielectric permittivity.

- Singh, J., Lo, T., Rudnick, D. R., Dorr, T. J., Burr, C. A., Werle, R., Shaver, T. M., & Muñoz-Arriola, F. (2018). **Performance assessment of factory and field calibrations for electromagnetic sensors in a loam soil**. Agricultural Water Management, 196, 87–98. https://doi.org/10.1016/j.agwat.2017.10.020

    The performance of eight electromagnetic (EM) sensors (TDR315, CS655, HydraProbe2, 5TE, EC5, CS616, Field Connect, AquaCheck), were analyzed through a field study in a loam soil. T, ECa, and ERa were compared in reference to overall average among all sensors, and ?v in reference to a neutron moisture meter (NMM).

- Lo, T. H., Rudnick, D. R., Singh, J., Nakabuye, H. N., Katimbo, A., Heeren, D. M., & Ge, Y. (2020). **Field assessment of interreplicate variability from eight electromagnetic soil moisture sensors**. Agricultural Water Management, 231(January), 105984. https://doi.org/10.1016/j.agwat.2019.105984

- Koley, S., & Jeganathan, C. (2020). **Estimation and evaluation of high spatial resolution surface soil moisture using multi-sensor multi-resolution approach**. Geoderma, 378(February), 114618. https://doi.org/10.1016/j.geoderma.2020.114618

- Serrano, D., Ávila, E., Barrios, M., Darghan, A., & Lobo, D. (2020). **Surface soil moisture monitoring with near-ground sensors: Performance assessment of a matric potential-based method**. Measurement: Journal of the International Measurement Confederation, 155, 107542. https://doi.org/10.1016/j.measurement.2020.107542

- Tan, W. Y., Then, Y. L., Lew, Y. L., & Tay, F. S. (2019). **Newly calibrated analytical models for soil moisture content and pH value by low-cost YL-69 hygrometer sensor**. Measurement: Journal of the International Measurement Confederation, 134, 166–178. https://doi.org/10.1016/j.measurement.2018.10.071

- Kizito, F., Campbell, C. S., Campbell, G. S., Cobos, D. R., Teare, B. L., Carter, B., & Hopmans, J. W. (2008). **Frequency, electrical conductivity and temperature analysis of a low-cost capacitance soil moisture sensor**. Journal of Hydrology, 352(3–4), 367–378. https://doi.org/10.1016/j.jhydrol.2008.01.021

- Santos, W. J. R., Silva, B. M., Oliveira, G. C., Volpato, M. M. L., Lima, J. M., Curi, N., & Marques, J. J. (2014). **Soil moisture in the root zone and its relation to plant vigor assessed by remote sensing at management scale**. Geoderma, 221–222(April), 91–95. https://doi.org/10.1016/j.geoderma.2014.01.006
