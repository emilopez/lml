
"""
Created on Wed Nov  8 12:13:28 2017

    Captor Library

@author: pauTE
"""

import numpy as np
import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D
import csv
import copy
from datetime import datetime
from numpy.linalg import inv
from sklearn import linear_model
from sklearn.neighbors import KNeighborsRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn import svm
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import GridSearchCV
from mlxtend.regressor import StackingRegressor


#%%
class Create_Data_Set():

    def __init__(self, FORMAT1 = "%d/%m/%Y %H:%M:%S", FORMAT2 = "%Y-%m-%dT%H:%M:%S",DEL1 = ";",DEL2 = ";"):
        self.FORMAT1 = FORMAT1
        self.FORMAT2 = FORMAT2
        self.DEL1 = DEL1
        self.DEL2 = DEL2

    def READ_DATA(self,file_cpt):
        '''
        This function reads a file_captor.csv file that contains the list
        of intervals (history) to filter and add to the DataSet.
        '''
        captors_in_file=[]
        dict_dates={}
        dict_location={}
        new_line=True
        with open(file_cpt, 'r') as fread:
            reader = csv.reader(fread,delimiter=';')
            for row in reader:
                if new_line == True:
                    captors_in_file=copy.deepcopy(row)
                    new_line=False
                    for k in range(len(captors_in_file)):
                        dict_dates[captors_in_file[k]] = []
                        dict_location[captors_in_file[k]] = []
                else:
                    dict_dates[row[0]].append([row[1],row[2]])
                    dict_location[row[0]].append(row[3])
        return captors_in_file,dict_dates,dict_location

    def READ_CLEAN_DATA(self,datafile):
        '''
        This function takes a datafile with samples and creates a new one
        without nan values.
        '''
        with open(datafile) as f:
            ncols = len(f.readline().split(';'))
        print(ncols)
        Data = []
        with open(datafile) as f:
            reader=csv.reader(f,delimiter=';')
            next(f)
            for row in reader:
                row_p = []
                row_p.append(row[0])
                ncol = len(row)
                lista_1 = [s for s in row[1:ncol]]
                cond_1 = True
                for item in lista_1:
                    if item =='':
                        cond_1 = False
                if cond_1:
                    lista = [float(s) for s in row[1:ncol]]
                    row_p.extend(lista)
                    Data.append(row_p)
        return Data


    def READ_ORG_DATA(self,datafile):
        '''
        This function takes a datafile with samples and creates a new one
        without nan and -1.0000 values. It is to say, remove any row that
        contains a nan or a -1.0000
        '''
        with open(datafile) as f:
            ncols = len(f.readline().split(';'))
        print(ncols)
        Data = []
        with open(datafile) as f:
            reader=csv.reader(f,delimiter=';')
            next(f)
            for row in reader:
                row_p = []
                row_p.append(row[0])
                ncol = len(row)
                lista_1 = [s for s in row[1:ncol-2]]
                cond_1 = True
                for item in lista_1:
                    if item =='':
                        cond_1 = False
                if cond_1:
                    lista = [float(s) for s in row[1:ncol-2]]
                cond_2 = True
                for item in lista:
                    if item == -1.0:
                        cond_2 = False
                if cond_1 and cond_2:
                    row_p.extend(lista)
                    Data.append(row_p)
        return Data


    def CAT_Files(self,dir_ER,col_f1,date_intervals,loc_list,FORMAT_file):
        '''
        This function takes a datafile with Ref Station values in the
        intervals specified by date_intervals.
        '''
        date_int_format=[]
        for i in range(len(date_intervals)):
            tmp=[]
            tmp.append(datetime.strptime(date_intervals[i][0],FORMAT_file))
            tmp.append(datetime.strptime(date_intervals[i][1],FORMAT_file))
            date_int_format.append(tmp)
        cat_list=[]
        for j in range(len(date_int_format)):
            f_name = dir_ER + loc_list[j] + "-data.csv"
            with open(f_name, 'r') as fread:
                reader = csv.reader(fread,delimiter=";")
                for row in reader:
                    strtime=datetime.strptime(row[0],FORMAT_file)
                    if date_int_format[j][0] <= strtime <=  date_int_format[j][1]:
                        tmp = []
                        tmp.append(row[0])
                        for rr in col_f1:
                            tmp.append(row[rr])
                        tmp.append(loc_list[j])
                        cat_list.append(tmp)
        return cat_list

    def merge_data(self,rd_file1,rd_file2, col_f1,col_f2, FORMAT_f1,FORMAT_f2,DEL1,DEL2,date_intervals):
        days_month=[31,28,31,30,31,30,31,31,30,31,30,31]
        data_f1=[]
        date_int_format=[]
        for i in range(len(date_intervals)):
            tmp=[]
            tmp.append(datetime.strptime(date_intervals[i][0],FORMAT_f1))
            tmp.append(datetime.strptime(date_intervals[i][1],FORMAT_f1))
            date_int_format.append(tmp)

        with open(rd_file1, 'r') as fread:
            reader = csv.reader(fread,delimiter=DEL1)
            next(reader,None) #skyp header
            for row in reader:

                strtime=datetime.strptime(row[0],FORMAT_f1)
                year=strtime.date().year
                month=strtime.date().month
                day=strtime.date().day
                hour=strtime.time().hour
                minute=strtime.time().minute

                t= (int(year)-2015)*8784+(month-1.0)*days_month[month-1]*24.0+day*24.0+hour+minute/60.0
                in_list=False
                for i in range(len(date_int_format)):
                    if date_int_format[i][0] <= strtime <=  date_int_format[i][1]:
                        in_list=True
                if in_list:
                    tmp=[]
                    tmp.append(t)
                    for j in range(len(col_f1)):
                        tmp.append(float(row[col_f1[j]]))
                    tmp.append(row[0])
                    data_f1.append(tmp)

        data_f2=[]

        with open(rd_file2, 'r') as fread:
            reader = csv.reader(fread,delimiter=DEL2)
            next(reader,None)
            for row in reader:
                if row[0]!="fields[0]":
                    strtime=datetime.strptime(row[0],FORMAT_f2)
                    year=strtime.date().year
                    month=strtime.date().month
                    day=strtime.date().day
                    hour=strtime.time().hour
                    minute=strtime.time().minute
                    t= (int(year)-2015)*8784+(month-1.0)*days_month[month-1]*24.0+day*24.0+hour+minute/60.0

                    tmp=[]
                    tmp.append(t)
                    for j in range(len(col_f2)):
                        print 'UPPS',row[0],row[col_f2[j]]
                        tmp.append(float(row[col_f2[j]]))
                    tmp.append(row[0])
                    data_f2.append(tmp)

        max_len=max(len(data_f2),len(data_f1))
        date_merge=[]
        time_merge=[]
        data_merge_1=[]
        data_merge_2=[]

        n=0
        m=0
        for i in range(0,2*max_len):
            if (n < len(data_f1)) and (m < len(data_f2)):
                if data_f1[n][0] == data_f2[m][0]:
                    date_merge.append(data_f1[n][len(col_f1)+1])
                    time_merge.append(float(data_f1[n][0]))
                    tmp=[]
                    for j in range(len(col_f1)):
                        tmp.append(float(data_f1[n][j+1]))
                    data_merge_1.append(tmp)
                    tmp=[]
                    for j in range(len(col_f2)):
                        tmp.append(float(data_f2[m][j+1]))
                    data_merge_2.append(tmp)
                    m+=1
                    n+=1
                else:
                    if data_f1[n][0]<data_f2[m][0]:
                        n+=1
                    else:
                        m+=1

        return date_merge,time_merge,data_merge_1,data_merge_2



#%%
class DataSet:

    def __init__(self, TRAIN_PERCENTAGE = 0.65):
        self.TRAIN_PERCENTAGE = TRAIN_PERCENTAGE

    def READ_DATA(self,datafile):
        '''
        This function takes a datafile with samples and creates a list:
        each item has the columns of the CSV file (being the first one
        the date and the rest sensor's readings, e.g. O3, T, HR).
        '''
        with open(datafile) as f:
            ncols = len(f.readline().split(';'))
        print(ncols)
        Data = []
        with open(datafile) as f:
            next(f)
            reader=csv.reader(f,delimiter=';')
            for row in reader:
                row_p = []
                row_p.append(row[0])
                lista = [float(s) for s in row[1:]]
                row_p.extend(lista)
                Data.append(row_p)
        return Data

    def READ_DATA_RefSt(self,datafile):
        '''
        This function takes a datafile with samples and creates a list:
        each item has the columns of the CSV file (being the first one
        the date and the rest sensor's readings, e.g. O3, T, HR).
        '''
        with open(datafile) as f:
            ncols = len(f.readline().split(';'))
        print(ncols)
        Data = []
        with open(datafile) as f:
            next(f)
            reader=csv.reader(f,delimiter=';')
            for row in reader:
                row_p = []
                row_p.append(row[0])
                lista = [float(s) for s in row[1:-1]]
                row_p.extend(lista)
                row_p.extend([row[-1]])
                Data.append(row_p)
        return Data


    def READ_DATA_DAYS(self,datafile):
        '''
        This function takes a datafile with samples and creates a list:
        each item has the columns of the CSV file (being the first one
        the date and the rest sensor's readings, e.g. O3, T, HR).
        '''
        with open(datafile) as f:
            ncols = len(f.readline().split(';'))
        print(ncols)
        Data = []
        with open(datafile) as f:
#            next(f)
            reader=csv.reader(f,delimiter=';')
            for row in reader:
                row_p = []
                row_p.append(row[0])
                lista = [float(s) for s in row[1:ncols-1]]
                row_p.extend(lista)
                row_p.append(float(row[ncols-1]))
                Data.append(row_p)
        return Data

    def MAP_DAYS(self,data,year):
        '''
        This function creates a map of samples per day.
        It returns the day number with respect 01/01/year.
        For example a sample at 02/02/2017 would return day = 31+2 = 33
        '''
        DATE_FORMAT = "%d/%m/%Y %H:%M"
#        DATE_FORMAT = "%d/%m/%Y %H:%M:%S"
        Map_Data = []
        for row in range(len(data)):
            date_time_struct=datetime.strptime(data[row][0],DATE_FORMAT)
#            day = (date_time_struct - datetime(int(year),1,1)).days + 1
            day = date_time_struct.timetuple().tm_yday
#            print 'day',day,'tuple',date_time_struct.timetuple().tm_yday
            Map_Data.append(day)
        return Map_Data

    def MAP_DAYS_VOL(self,data,year):
        '''
        This function creates a map of samples per day.
        It returns the day number with respect 01/01/year.
        For example a sample at 02/02/2017 would return day = 31+2 = 33
        '''
#        DATE_FORMAT = "%d/%m/%Y %H:%M"
        DATE_FORMAT = "%d/%m/%Y %H:%M:%S"
        Map_Data = []
        for row in range(len(data)):
            date_time_struct=datetime.strptime(data[row][0],DATE_FORMAT)
#            day = (date_time_struct - datetime(int(year),1,1)).days + 1
            day = date_time_struct.timetuple().tm_yday
#            print 'day',day,'tuple',date_time_struct.timetuple().tm_yday
            Map_Data.append(day)
        return Map_Data


    def COUNT_DAYS(self,m_day):
        '''
        This function returns the number of samples per day.
        If a day is left, then returns a 1 for that day
        '''
        c_day = []
        mini = np.min(m_day)
        maxi = np.max(m_day)
        k=0
        j=mini
        print 'day mini', mini, 'day maxi',maxi, 'total: ',len(m_day)
        while (j<maxi):
            if m_day.count(m_day[k]) >0:
                c_day.append(m_day.count(m_day[k]))
#                print m_day[k],m_day.count(m_day[k]),np.sum(c_day)
            k=k+m_day.count(m_day[k])
            if j<maxi:
                j = m_day[k]
            if j< m_day[k]:
                j = j+1
        c_day.append(m_day.count(m_day[k]))
#        print m_day[k],m_day.count(m_day[k]),np.sum(c_day)
        return c_day

    def COUNT_DAYS_DICT(self,m_day):
        '''
        This function returns the number of samples per day.
        If a day is left, then returns a 1 for that day
        '''
        c_day = []
        mini = np.min(m_day)
        maxi = np.max(m_day)
        k=0
        j=mini
        print 'day mini', mini, 'day maxi',maxi, 'total: ',len(m_day)
        while (j<maxi):
            if m_day.count(m_day[k]) >0:
                c_day.append(m_day.count(m_day[k]))
#                print m_day[k],m_day.count(m_day[k]),np.sum(c_day)
            k=k+m_day.count(m_day[k])
            if j<maxi:
                j = m_day[k]
            if j< m_day[k]:
                j = j+1
        c_day.append(m_day.count(m_day[k]))
#        print m_day[k],m_day.count(m_day[k]),np.sum(c_day)
        days_l = self.GIVE_LIST_of_DAYS(c_day,m_day)
        c_day_dict = dict()
        for i in range(len(days_l)):
            c_day_dict[days_l[i]] = c_day[i]
        return c_day_dict


    def COUNT_WEEKS(self,d_list,c_day):
        '''
        This function returns the number of samples per week
        in a dictionary.
        '''
        mini = np.min(d_list)/7
        maxi = np.max(d_list)/7
        print 'week mini', mini, 'week maxi',maxi, 'total: ',len(d_list)
        c_week = dict()
        week = mini
        k=mini*7
        while (week<=maxi):
            aux = 0
#            print 'Week: ', week
            while (k<(week+1)*7):
                if k in d_list:
                    index = d_list.index(k)
                    aux = aux + c_day[index]
#                    print 'day', k,index,c_day[index],aux
                k = k + 1
            c_week[week] = aux
            week = week +1
            k = week*7

        return c_week

    def GIVE_LIST_of_DAYS(self,c_day,m_day):
        '''
        This function returns the number of days that appear in the samples.
        '''
        count_D=0
        day_list = []
        num_days = len(c_day)
        for j in range(num_days):
            day = m_day[count_D]
            day_list.append(day)
            count_D = count_D + c_day[j]
        return day_list

    def COUNT_DAYS_2(self,m_day):
        '''
        This function returns the number of samples per day.
        If a day is left, then returns a 1 for that day
        '''
        c_day = []
        n_day=[]
        mini = np.min(m_day)
        maxi = np.max(m_day)
        k=0
        j=mini
        while (j<maxi):
            if m_day.count(m_day[k]) >0:
                c_day.append(m_day.count(m_day[k]))
            k=k+m_day.count(m_day[k])
            n_day.append(m_day[k])
            if j<maxi:
                j = m_day[k]
            if j< m_day[k]:
                j = j+1
        c_day.append(m_day.count(m_day[k]))
        n_day.append(m_day[k])
        return c_day,n_day

    def NORMALIZE(self,t_Data,t_Mean,t_Sd):
        '''
        This function normalizes the data with respect a mean and std.
        '''
        t_Norm=np.zeros(t_Data.shape)
        for i in range(0,t_Data.shape[1]):
            if t_Sd[i]!=0.:
                t_Norm[:,i]=(t_Data[:,i]-t_Mean[i])/t_Sd[i]
            else:
                t_Norm[:,i]=t_Data[:,i]
        return t_Norm

    def WEEK_DATA(self,dataset,t_weeks,WEEK):
        '''
        This function obtains the data for the specified weeks.
        dataset is a numpy.ndarray (ALLDATA)
        weeks is a list with the weeks to be taken
        WEEK is the length of a week in samples
        '''
        d_set =np.zeros((len(t_weeks)*WEEK,dataset.shape[1]))
        for i in range(len(t_weeks)):
            d1= t_weeks[i] * WEEK
            d2 = d1 + WEEK
            t1= i * WEEK
            t2 = t1 + WEEK
            d_set[t1:t2,:] = dataset[d1:d2,:]
#            print 'd1',d1,'d2',d2
        return d_set

    def WEEK_DATA_DAYS(self,dataset,t_weeks,c_days,size,WEEK):
        '''
        This function obtains the data for the specified weeks.
        dataset is a numpy.ndarray (ALLDATA)
        weeks is a list with the weeks to be taken
        WEEK is the length of a week in samples
        '''
        print 'size',size,'shape',dataset.shape[1]

        d_set =np.zeros((size,dataset.shape[1]))
        t1 = 0
        for i in t_weeks:
            d1 = int(np.sum(c_days[0:i*WEEK]))
            d2 = int(np.sum(c_days[0:(i+1)*WEEK]))
#            print 'd1',d1,'d2',d2
            t2 = t1 + d2 - d1
#            print 't1',t1,'t2',t2
            d_set[t1:t2,:] = dataset[d1:d2,:]
            t1 = t2
        return d_set


    def DATA_DICT_ER(self,datafile,ER,type_ER,Temp,HR):
        '''
        This function creates a dictionary with the datafile.
        '''
        tr_dict=dict()
        for i in range(len(ER)):
            captor_ER = type_ER[i] + '_ER'
            tr_dict[captor_ER]= datafile[:,i]  # vector with O3_PR
        tr_dict["HR_Captor"]=datafile[:,HR]  # vector with HR captor node
        tr_dict["Temp_Captor"]=datafile[:,Temp]  # vector with Temp captor node
        return tr_dict

    def DATA_DICT_Sens(self,datafile,tr_dict,type_s,sensors_cal):
        '''
        This function creates a dictionary with the datafile.
        '''
        for i in sensors_cal:
            captor= type_s + "_Captor_"+str(i)
            tr_dict[captor]=datafile[:,i]  # adding O3 of captor sensors
        return tr_dict


    def DATA_DICT(self,datafile,ER,sensors_cal,Temp,HR):
        '''
        This function creates a dictionary with the datafile.
        '''
        tr_dict=dict()
        tr_dict["O_PR"]= datafile[:,ER]  # vector with O3_PR
        tr_dict["HR_Captor"]=datafile[:,HR]  # vector with HR captor node
        tr_dict["Temp_Captor"]=datafile[:,Temp]  # vector with Temp captor node
        for i in sensors_cal:
            captor="O_Captor_"+str(i)
            tr_dict[captor]=datafile[:,i]  # adding O3 of captor sensors
        return tr_dict

    def errors(self,x_PD,y_Data,tMean,p_s):
        '''
        This function obtains the RMSE and R2 QoI metrics.
        '''
        error_vector_PD=(x_PD-y_Data)**2
        errorMSE_PD = np.sum(error_vector_PD)
        RMSE = np.sqrt(errorMSE_PD/(len(x_PD)-p_s+1))

        avg_vector_PD=(tMean-y_Data)**2
        avg = np.sum(avg_vector_PD)
        R2 = 1.0-errorMSE_PD/avg
        return RMSE,R2

    def errors_cut(self,x_PD,y_Data,tMean,p_s,LOW_LIMIT):
        '''
        This function obtains the RMSE and R2 QoI metrics.
        '''
        error_vector = []
        for i in range(len(x_PD)):
            if x_PD[i] >= LOW_LIMIT:
                error_vector.append(x_PD[i]-y_Data[i])
#        print 'ORG = ',len(x_PD), 'CUT = ', len(error_vector)
        error_vector = np.array(error_vector)
        error_vector_PD=(error_vector)**2
        errorMSE_PD = np.sum(error_vector_PD)
        RMSE = np.sqrt(errorMSE_PD/(len(error_vector)-p_s+1))
#==============================================================================
#
#         avg_vector_PD=(tMean-y_Data)**2
#         avg = np.sum(avg_vector_PD)
#         R2 = 1.0-errorMSE_PD/avg
#==============================================================================
        return RMSE

    def Data_Model(self,xvector,yvector):
        '''
        This function obtains a model.
        xvector = list with X variables
        yvector = the Reference station data
        '''
        X= np.matrix(xvector)
        X=np.transpose(X)
        Y=np.matrix(yvector)
        Y=np.transpose(Y)
        return X,Y

    def AVGSTD_MODEL_DICT(self,AllData,Blocks,AVG_SIZE,Temp,HR,nsen,sensors_cal):
        '''
        This function obtains a dictionary for
        the AVG-STD model.
        Data = Data for creating the AVG - STD blocks
        Block = block size
        '''
        ER_avg_AVG_SIZE = np.zeros((1,Blocks))
        ER_std_AVG_SIZE = np.zeros((1,Blocks))
        temp_avg_AVG_SIZE = np.zeros((1,Blocks))
        hr_avg_AVG_SIZE = np.zeros((1,Blocks))
        temp_std_AVG_SIZE = np.zeros((1,Blocks))
        hr_std_AVG_SIZE = np.zeros((1,Blocks))

        for j in range(Blocks):
            if j < Blocks:
                ER_avg_AVG_SIZE[0,j] = np.mean(AllData[j*AVG_SIZE:(j+1)*AVG_SIZE,0],axis=0)
                ER_std_AVG_SIZE[0,j] = np.std(AllData[j*AVG_SIZE:(j+1)*AVG_SIZE,0],axis=0)
                temp_avg_AVG_SIZE[0,j] = np.mean(AllData[j*AVG_SIZE:(j+1)*AVG_SIZE,Temp],axis=0)
                temp_std_AVG_SIZE[0,j] = np.std(AllData[j*AVG_SIZE:(j+1)*AVG_SIZE,Temp],axis=0)
                hr_avg_AVG_SIZE[0,j] = np.mean(AllData[j*AVG_SIZE:(j+1)*AVG_SIZE,HR],axis=0)
                hr_std_AVG_SIZE[0,j] = np.std(AllData[j*AVG_SIZE:(j+1)*AVG_SIZE,HR],axis=0)

        sensor_avg_AVG_SIZE = np.zeros((nsen,Blocks))
        sensor_std_AVG_SIZE = np.zeros((nsen,Blocks))
        s = -1
        for i in sensors_cal:
            s = s + 1
            for j in range(Blocks):
                sensor_avg_AVG_SIZE[s,j] = np.mean(AllData[j*AVG_SIZE:(j+1)*AVG_SIZE,i],axis=0)
                sensor_std_AVG_SIZE[s,j] = np.std(AllData[j*AVG_SIZE:(j+1)*AVG_SIZE,i],axis=0)

        mu_TRAIN = int(Blocks)
        m_tr_dict=dict()
        m_tr_dict["O_PR"] = ER_avg_AVG_SIZE[0,0:mu_TRAIN]
        m_tr_dict["Temp_Captor"] = temp_avg_AVG_SIZE[0,0:mu_TRAIN]
        m_tr_dict["HR_Captor"] = hr_avg_AVG_SIZE[0,0:mu_TRAIN]
        for i in range(len(sensors_cal)):
            captor="O_Captor_"+str(sensors_cal[i])
            m_tr_dict[captor] = sensor_avg_AVG_SIZE[i,:mu_TRAIN]

        sg_tr_dict=dict()
        sg_tr_dict["O_PR"] = ER_std_AVG_SIZE[0,0:mu_TRAIN]
        sg_tr_dict["Temp_Captor"] = temp_std_AVG_SIZE[0,0:mu_TRAIN]
        sg_tr_dict["HR_Captor"] = hr_std_AVG_SIZE[0,0:mu_TRAIN]
        for i in range(len(sensors_cal)):
            captor="O_Captor_"+str(sensors_cal[i])
            sg_tr_dict[captor] = sensor_std_AVG_SIZE[i,:mu_TRAIN]

        return m_tr_dict,sg_tr_dict

    def MLR_AVGSTD(self,m_tr_dict,s_tr_dict,nsen,sensors_cal,Blocks):
        '''
        This function obtains the coeficients (alphas) for a LR AVG-STD model
        Data = Data for creating the AVG - STD blocks
        Block = block size
        '''
        alphas_R = np.zeros((len(sensors_cal),2))
        s = -1
        mu_TRAIN = int(Blocks)
        for i in sensors_cal:
            Ofit="O_Captor_"+str(i)
            s=s+1

            X_m_tr_1, Y_m_tr_1 = self.Data_Model([np.ones(mu_TRAIN),m_tr_dict["Temp_Captor"]],s_tr_dict["O_PR"]/s_tr_dict[Ofit])
#            X_m_tr_1, Y_m_tr_1 = self.Data_Model([np.ones(mu_TRAIN),m_tr_dict["Temp_Captor"]/s_tr_dict[Ofit]],s_tr_dict["O_PR"]/s_tr_dict[Ofit])

            ## usando las ecuaciones normales
            m_alpha_tr = inv((X_m_tr_1.transpose()*X_m_tr_1))*(X_m_tr_1.transpose()*Y_m_tr_1)

            alphas_R[s,0] = m_alpha_tr[0,0]
            alphas_R[s,1] = m_alpha_tr[1,0]

        return alphas_R

    def MLR_AVGSTD_THR(self,m_tr_dict,s_tr_dict,nsen,sensors_cal,Blocks):
        '''
        This function obtains the coeficients (alphas) for a LR AVG-STD model
        Data = Data for creating the AVG - STD blocks
        Block = block size
        '''
        alphas_R = np.zeros((len(sensors_cal),3))
        s = -1
        mu_TRAIN = int(Blocks)
        for i in sensors_cal:
            Ofit="O_Captor_"+str(i)
            s=s+1

            X_m_tr_1, Y_m_tr_1 = self.Data_Model([np.ones(mu_TRAIN),m_tr_dict["Temp_Captor"],m_tr_dict["HR_Captor"]],s_tr_dict["O_PR"]/s_tr_dict[Ofit])
#            X_m_tr_1, Y_m_tr_1 = self.Data_Model([np.ones(mu_TRAIN),m_tr_dict["Temp_Captor"]/s_tr_dict[Ofit]],s_tr_dict["O_PR"]/s_tr_dict[Ofit])

            ## usando las ecuaciones normales
            m_alpha_tr = inv((X_m_tr_1.transpose()*X_m_tr_1))*(X_m_tr_1.transpose()*Y_m_tr_1)

            alphas_R[s,0] = m_alpha_tr[0,0]
            alphas_R[s,1] = m_alpha_tr[1,0]
            alphas_R[s,2] = m_alpha_tr[2,0]

        return alphas_R

    def MLR_AVGSTD_INV(self,m_tr_dict,s_tr_dict,nsen,sensors_cal,Blocks):
        '''
        This function obtains the coeficients (alphas) for a LR AVG-STD model
        Data = Data for creating the AVG - STD blocks
        Block = block size
        '''
        alphas_R = np.zeros((len(sensors_cal),2))
        s = -1
        mu_TRAIN = int(Blocks)
        for i in sensors_cal:
            Ofit="O_Captor_"+str(i)
            s=s+1

            X_m_tr_1, Y_m_tr_1 = self.Data_Model([np.ones(mu_TRAIN),m_tr_dict["Temp_Captor"]],s_tr_dict[Ofit]/s_tr_dict["O_PR"])

            ## usando las ecuaciones normales
            m_alpha_tr = inv((X_m_tr_1.transpose()*X_m_tr_1))*(X_m_tr_1.transpose()*Y_m_tr_1)

            alphas_R[s,0] = m_alpha_tr[0,0]
            alphas_R[s,1] = m_alpha_tr[1,0]

        return alphas_R

    def MLR_MultiScale(self,x_PD_1,Data_1,DATA_SIZE,AVG_SIZE):
        '''
        This function calculates the re-scaling
        Data = Data for creating the AVG - STD blocks
        Block = block size
        '''
        x_PD_11 = np.zeros(DATA_SIZE)
        mu_MS = np.zeros(DATA_SIZE/AVG_SIZE)
        sigma_MS = np.zeros(DATA_SIZE/AVG_SIZE)
        mu_SS = np.zeros(DATA_SIZE/AVG_SIZE)
        sigma_SS = np.zeros(DATA_SIZE/AVG_SIZE)

        A_1 = np.zeros(DATA_SIZE/AVG_SIZE)
        A_2 = np.zeros(DATA_SIZE/AVG_SIZE)

        for j in range(DATA_SIZE/AVG_SIZE):
            mu_SS[j] = np.mean(x_PD_1[j*AVG_SIZE:(j+1)*AVG_SIZE])
            sigma_SS[j] = np.std(x_PD_1[j*AVG_SIZE:(j+1)*AVG_SIZE])

            mu_MS[j] =  np.mean(Data_1[j*AVG_SIZE:(j+1)*AVG_SIZE])
            sigma_MS[j] = np.std(Data_1[j*AVG_SIZE:(j+1)*AVG_SIZE])

            A_1[j] =  sigma_MS[j]/sigma_SS[j]
            A_2[j] =  mu_MS[j]  - A_1[j] * mu_SS[j]

            x_PD_11[j*AVG_SIZE:(j+1)*AVG_SIZE] = A_2[j] + A_1[j] * x_PD_1[j*AVG_SIZE:(j+1)*AVG_SIZE]

        mu_SS[j] = np.mean(x_PD_1[j*AVG_SIZE:DATA_SIZE])
        sigma_SS[j] = np.std(x_PD_1[j*AVG_SIZE:DATA_SIZE])

        mu_MS[j] =  np.mean(Data_1[j*AVG_SIZE:(j+1)*AVG_SIZE])
        sigma_MS[j] = np.std(Data_1[j*AVG_SIZE:(j+1)*AVG_SIZE])

        A_1[j] =  sigma_MS[j]/sigma_SS[j]
        A_2[j] =  mu_MS[j]  - A_1[j] * mu_SS[j]

        x_PD_11[j*AVG_SIZE:DATA_SIZE] = A_2[j] + A_1[j] * x_PD_1[j*AVG_SIZE:DATA_SIZE]

        return x_PD_11

    def MLR_MeansStds(self,Data,sensor,t_mean,t_std):
        '''
        This function calculates the MLR for Means and STDs
        '''

        NormMean = self.NORMALIZE(Data,t_mean,t_std)
        Norm_Mean = self.DATA_DICT(NormMean,sensor[0],[sensor[1]],sensor[2],sensor[3])

        gamm = np.zeros(4)
        Ofit = "O_Captor_" + str(sensor[1])
        X_des=np.matrix([np.ones(len(Norm_Mean[Ofit])),Norm_Mean[Ofit],Norm_Mean["Temp_Captor"],Norm_Mean["HR_Captor"]])
        X_des=np.transpose(X_des)
        Y_des=np.matrix(Norm_Mean["O_PR"])
        Y_des=np.transpose(Y_des)
        regr = linear_model.LinearRegression()
        regr.fit(X_des, Y_des)
        gamma=regr.coef_
        gamm[0] = regr.intercept_
        gamm[1] = gamma[0][1]
        gamm[2] = gamma[0][2]
        gamm[3] = gamma[0][3]

        return gamm

    def MLR_MeansStds_NoNorm(self,Data,sensor,t_mean,t_std):
        '''
        This function calculates the MLR for Means and STDs
        '''

        Norm_Mean = self.DATA_DICT(Data,sensor[0],[sensor[1]],sensor[2],sensor[3])

        gamm = np.zeros(4)
        Ofit = "O_Captor_" + str(sensor[1])
        X_des=np.matrix([np.ones(len(Norm_Mean[Ofit])),Norm_Mean[Ofit],Norm_Mean["Temp_Captor"],Norm_Mean["HR_Captor"]])
        X_des=np.transpose(X_des)
        Y_des=np.matrix(Norm_Mean["O_PR"])
        Y_des=np.transpose(Y_des)
        regr = linear_model.LinearRegression()
        regr.fit(X_des, Y_des)
        gamma=regr.coef_
        gamm[0] = regr.intercept_
        gamm[1] = gamma[0][1]
        gamm[2] = gamma[0][2]
        gamm[3] = gamma[0][3]

        return gamm

    def train_tune_KNN(self, X_des, Y_des):

        """ This function performs parameter tunning for SVR
          and trains the model with the best set of parameters """
        kfold = 10
        neighs = np.arange(start = 1, stop = 50, step = 2)
        ps = np.arange(start = 1, stop = 9, step = 1)
        param_grid = { 'n_neighbors' : neighs, 'p': ps}
        knn_estimator = KNeighborsRegressor()
        search = GridSearchCV(knn_estimator, param_grid,scoring = 'neg_mean_squared_error', cv = kfold, refit = False )
        search.fit(X_des, Y_des)
        cv_error = search.best_score_
        best_k = search.best_params_['n_neighbors']
        best_p = search.best_params_['p']
        knn_estimator = KNeighborsRegressor(n_neighbors = best_k, p = best_p )
        knn_estimator.fit(X_des, np.ravel(Y_des))

        return knn_estimator, [best_k, best_p, cv_error]

    def train_tune_RF(self, X_des, Y_des):

        """ This function performs parameter tunning for SVR
          and trains the model with the best set of parameters """
        kfold = 10

        n_estimators = [16 , 32, 50, 100, 250, 500, 660, 1000]
        max_features = [1,2,3]
        max_depth = [3,5,7,10]
        param_grid = { 'n_estimators' : n_estimators, 'max_features': max_features,
                      'max_depth': max_depth}
        rf_estimator = RandomForestRegressor()
        search = GridSearchCV(rf_estimator, scoring = 'neg_mean_squared_error', param_grid = param_grid, cv = kfold, refit = False )
        search.fit(X_des, np.ravel(Y_des))
        cv_error = search.best_score_
        best_n = search.best_params_['n_estimators']
        best_features = search.best_params_['max_features']
        best_depth = search.best_params_['max_depth']
        rf_estimator = RandomForestRegressor(n_estimators = best_n,
                                             max_features = best_features,
                                             max_depth = best_depth)
        rf_estimator.fit(X_des, np.ravel(Y_des))
        return rf_estimator, [best_n, best_features, best_depth, cv_error]

    def train_tune_SVR(self, X_des, Y_des):

        """ This function performs parameter tunning for SVR
          and trains the model with the best set of parameters """
        kfold = 10
        cs = [1.0, 10.0, 1000.0]
        gs = np.arange(start=0.1, stop = 2.0, step=0.2)
        epsilons = np.arange( start = 0.05, stop = 0.25, step = 0.05)
        param_grid = { 'C' : cs, 'gamma': gs, 'epsilon': epsilons }
        #The impact of the gamma parameter is much higer than the cost parameter
        # so we check more values of gamma
        # We must also tune the epsilon from epsilon-insensitive loss function
        svr_estimator = svm.SVR()
        search = GridSearchCV(svr_estimator, scoring = 'neg_mean_squared_error',param_grid = param_grid, cv = kfold, refit = True )
        search.fit(X_des, Y_des)
        cv_error = search.best_score_
        best_c = search.best_params_['C']
        best_gamma = search.best_params_['gamma']
        best_eps = search.best_params_['epsilon']
        svr_estimator = svm.SVR(C = best_c, gamma = best_gamma,
                                epsilon = best_eps)
        svr_estimator.fit(X_des, np.ravel(Y_des))

        return search, [best_c, best_gamma, best_eps, cv_error]

    def train_tune_Stacking(self, X_des, Y_des):
        #Knn params
        neighs = np.arange(start = 1, stop = 50, step = 2)
        ps = np.arange(start = 1, stop = 9, step = 1)
        # Svr params
        kfold = 10
        # RF params
        n_estimators = [50, 100, 250, 660, 1000]
        # MOdel definition
        mlr_estimator = linear_model.LinearRegression()
        knn_estimator = KNeighborsRegressor()
        rf_estimator = RandomForestRegressor()
        meta_knn = KNeighborsRegressor()
        regressors = [mlr_estimator, knn_estimator, rf_estimator]
    	# regressors = [mlr_estimator, knn_estimator]
    	stregr = StackingRegressor(regressors=regressors,
                               meta_regressor= meta_knn)
    	p = X_des.shape[1]
        max_features = np.arange(start = 1, stop = 3, step = 1)
        max_depth = [3,5,7]
        params = {'kneighborsregressor__n_neighbors' : neighs,
        'kneighborsregressor__p': ps,
        'randomforestregressor__n_estimators': n_estimators,
        'randomforestregressor__max_depth': max_depth,
        'randomforestregressor__max_features': max_features,
         'meta-kneighborsregressor__n_neighbors': [13, 21, 41 ]}
        search = GridSearchCV(estimator=stregr,
                            param_grid=params,
                            cv=5,
                            refit=True, n_jobs = 4)
        search.fit(X_des, np.ravel(Y_des))

        return search, []



#%%
class Utils_Cal:

    def removeBadObservations(self,dataset):
        '''
        This function removes rows from the datset having -1 on a sensor
        '''
        aux = dataset.copy()
        aux = dataset[dataset[:,1]!=-1.0,]
        aux = aux[aux[:,2]!=-1.0,]
        aux = aux[aux[:,3]!=-1.0,]
        aux = aux[aux[:,4]!=-1.0,]
        aux = aux[aux[:,5]!=-1.0,]
        return aux

    def selectLocation(self,dataset,refs,place):
        '''
        This function select the captor data from a palce and removes bad obsevrations
        (i.e. data from PR, then place ='PR' and refs is all the labels)
        '''
        indexes = np.where(refs==place)[0]
        dataset = dataset[indexes,:]
        dataset = self.removeBadObservations(dataset)
        return dataset

    def shift(self,a,step):
        '''
        This function shifts the data if this is not UTC with respect ER data.
        '''
        ee=[0.0]*len(a)
        for t in range(len(a)):
            ee[t]=a[t]
        if step>0:
            for t in range(step,len(a)):
                ee[t]=a[t-step]
        if step<0:
            for t in range(len(a)+step):
                ee[t]=a[t-step]
        return ee

    def adjustedRsquared(self,rsquared,n,k):
        '''
        This function computes the adjusted rsquared given the number
        of observations and the numbe rof features used in the fit
        '''
        intermidiate = (float(k-1.0)/float(n-k))*(1.0-rsquared)
        return  rsquared - intermidiate


    def drift_set(self,dataset,N,dir_wr):
        '''
        This functions separates a dataset in several datasets of N samples each
        '''
        num = len(dataset)/N
        resto = len(dataset) - num *len(dataset)
        ini = 0
        for i in range(num):
            file_w = dir_wr + "node_" + str(num+1) + ".csv"
            with open(file_w,"w") as fw:
                writer=csv.writer(fw,delimiter=';')
                for j in range(ini,ini+N):
                    writer.writerow(dataset[j])
            ini = ini + N
        if resto > 0:
            file_w = dir_wr + "node_" + str(num+1) + ".csv"
            with open(file_w,"w") as fw:
                writer=csv.writer(fw,delimiter=';')
                for j in range(ini,ini+len(dataset)):
                    writer.writerow(dataset[j])
        return num+1

#%%
class Plots_Cal:

    def __init__(self,date_in,date_end,node_id,place):
        self.color = ['black','red','blue','magenta','green','orange']
        self.date_in = str(date_in)
        self.date_end = str(date_end)
        self.node_id = node_id     ## CAPTOR number
        self.place = place
        self.sensors = ['ER','s1','s2','s3','s4','s5']

    def PLOT_NORM_DATA_SENSORS(self,NormDataSet,file_wr,sens,saved,ylabel):
        '''
        This figure plots the normalized data for the n sensors
        sens = [0,1,2,3,4,5] or [0,3] indicates the columns to be plotted
        saved = TRUE or FALSE if wanted to be saved in a file
        '''
        plt.ion()
        plt.figure(1)
        ll=0
        ul=len(NormDataSet)
        x = np.arange(ll, ul, 1)
        for i in sens:
            plt.plot(x,NormDataSet[:ul,i],color=self.color[i],linewidth=2.0, linestyle="-",label=self.sensors[i])
        plt.ylabel('Normalized' + ylabel + ' values',fontsize=13)
        text = self.node_id + ': Number of samples (1 sample each hour)'
        plt.xlabel(text,fontsize=13)
        plt.legend(loc='upper left')
        plt.title(self.place+" - Dates, from:"+self.date_in+" to "+self.date_end)
        if saved:
            plt.savefig(file_wr)
        plt.show()
        plt.close(1)

    def PLOT_DATA_SENSORS(self,DataSet,file_wr,sens,saved,ylabel):
        '''
        This figure plots the data for the n sensors
        sens = [0,1,2,3,4,5] or [0,3] indicates the columns to be plotted
        saved = TRUE or FALSE if wanted to be saved in a file
        '''
        plt.ion()
        plt.figure(2)
        ll=0
        ul=len(DataSet)
        x = np.arange(ll, ul, 1)
        for i in sens:
            plt.plot(x,DataSet[:ul,i],color=self.color[i],linewidth=2.0, linestyle="-",label=self.sensors[i])
        plt.ylabel('Resistor measure of an' + ylabel + ' sensor (KOhm)',fontsize=13)
        text = self.node_id + ': Number of samples (1 sample each hour)'
        plt.legend(loc='upper left')
        plt.xlabel(text,fontsize=13)
        plt.title(self.place+" - Dates, from:"+self.date_in+" to "+self.date_end)
        if saved:
            plt.savefig(file_wr)
        plt.show()
        plt.close(2)

    def SCATTERPLOT_NORM_DATA_SENSORS(self,DataS_RS,DataS_Sn,file_wr,sensor,saved,ylabel):
        '''
        This figure plots the data for the n sensors
        DataS_RS is the Ref Station data
        DataS_Sn is the sensor data
        sensor =  indicates the sensor to be scatter-plotted
        saved = TRUE or FALSE if wanted to be saved in a file
        '''
        plt.ion()
        plt.figure(3)
        plt.scatter(DataS_Sn,DataS_RS,color=self.color[sensor],label='s'+str(sensor))
        plt.xlabel(self.node_id + ': Sensor' + ylabel + ' (normalized data)',fontsize=13)
        plt.ylabel('Reference node' + ylabel + ' (normalized data)',fontsize=13)
        plt.legend(loc='upper left')
        if saved:
            plt.savefig(file_wr)
        plt.show()
        plt.close(3)

    def SCATTERPLOT_NORM_DATA_SENSORS_2_colors(self,DataS_RS1,DataS_RS2,DataS_Sn1,DataS_Sn2,file_wr,sensor,saved,xlabel):
        '''
        This figure plots the data for the n sensors
        DataS_RS is the Ref Station data
        DataS_Sn is the sensor data
        sensor =  indicates the sensor to be scatter-plotted
        saved = TRUE or FALSE if wanted to be saved in a file
        '''
        plt.ion()
        plt.figure(3)
        plt.scatter(DataS_Sn1,DataS_RS1,color=self.color[1],label='s'+str(sensor)+' (Training)')
        plt.scatter(DataS_Sn2,DataS_RS2,color=self.color[2],label='s'+str(sensor)+' (Testing)')
        plt.xlabel('Sensor Resistor (KOhm)' ,fontsize=13)
        plt.ylabel('Ozone concentration ($\mu$gr/m$^3$) Reference node',fontsize=13)
        plt.legend(loc='upper left')
        if saved:
            plt.savefig(file_wr)
        plt.show()
        plt.close(3)

    def SCATTERPLOT_DATA_SENSORS_2_colors(self,DataS_RS1,DataS_RS2,DataS_Sn1,DataS_Sn2,file_wr,sensor,saved,ylabel):
        '''
        This figure plots the data for the n sensors
        DataS_RS is the Ref Station data
        DataS_Sn is the sensor data
        sensor =  indicates the sensor to be scatter-plotted
        saved = TRUE or FALSE if wanted to be saved in a file
        '''
        plt.ion()
        plt.figure(3)
        plt.scatter(DataS_Sn1,DataS_RS1,color=self.color[1],label='s'+str(sensor)+'Cal1')
        plt.scatter(DataS_Sn2,DataS_RS2,color=self.color[2],label='s'+str(sensor)+'Cal2')
        plt.xlabel(self.node_id + ': Sensor' + ylabel + ' (Non-normalized data)',fontsize=13)
        plt.ylabel('Reference node' + ylabel + ' (Non-normalized data)',fontsize=13)
        plt.legend(loc='upper left')
        if saved:
            plt.savefig(file_wr)
        plt.show()
        plt.close(3)


    def PLOT_CALIB_DATA_SENSORS(self,x,ER_data,S_data,file_wr,sensor,labels,saved,max_y):
        '''
        This figure plots the calibrated (training, test or All-data) data for the n sensors
        ER_data is the Ref Station data
        S_data is the sensor data
        labels = labels to be printed [xlabel,title]
        saved = TRUE or FALSE if wanted to be saved in a file
        '''
        plt.figure(4)
        plt.ion()
        plt.plot(x,ER_data,color=self.color[0],linewidth=1.75, linestyle="-",label=self.place)
        plt.plot(x,S_data,color=self.color[labels[3]], linewidth=1.75, linestyle="-",label=self.node_id+self.sensors[sensor])
        plt.ylabel(labels[2] +' $\mu$gr/m$^3$')
        plt.ylim(0,max_y+10)
        plt.xlabel(labels[0],fontsize=13)
        plt.legend(loc='lower right')
        plt.title(labels[1],fontsize=13)
        if saved:
            plt.savefig(file_wr,bbox_inches='tight')
        plt.show()
        plt.close(4)

    def plot3D(self,dataset,o3_sensor = 1):
        '''
        This functions plots a 3D plot of an ozone sensor against
        the HR and Temp
        '''
        plt.ion()
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(dataset[:,o3_sensor],dataset[:,5],dataset[:,6])
        ax.set_xlabel('O3')
        ax.set_ylabel('Temp')
        ax.set_zlabel('HR')
        plt.show()
