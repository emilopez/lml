[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/emilopez%2Flml/HEAD)

# Estancia en UPC / Barcelona

## Python issues
### Ver vitual environment en jupyter
$ conda activate lml           
(lml)$ conda install ipykernel
(lml)$ ipython kernel install --user --name=lml

### Versiones de paquetes

- python: 3.7.13
- scikit-learn: 0.24.1
- pandas: 1.2.1
- pickle5: 0.0.11
- plotly: 5.9.0
- nbformat: 5.4.0

IMPORTANTE: agregar en /home/emiliano/anaconda3/envs/lml/lib/python3.7/site-packages/pandas/io/pickle.py 
import pickle3 as pickle

## Todo list
- [x] plot a MLR hyperplane
- [x] plot a hyperplanes generated with nonlinear methods
- [x] take a look at *Best subset selection* and *Random Forest Feature Importance* (Pau's recommendation)
- [x] predict with new data (soil moisture dateset 2 and 3)
- [ ] 

## Pre-processing

- `0_clean-raw-data.ipynb`: ensable the measurments in a tabular CSV file -> saved in `soilmoistureML0.csv`
- `1_clean-outliers.ipynb`: remove outliers-> results saved in `data/soilmoistureML1.csv`

## Regressions and plots

- `2_regressions.ipynb`: multiple linear regression, k-nearest neigbhor, random forest and support vector regression. 

## Log
- 2021-01-14; llego a BCN!
- 2021-01-15 15hs; Reunión virual 
- 2021-01-21 10.30hs; Presentación en UPC . Tareas acordadas: Leer papers, Leer tesis de master de Pau Ferrer-Cid, Practicar con sus datos y los mios los algoritmos de ML usados por ellos
- 2021-01-22; Lectura de paper: P. Ferrer-Cid, J. M. Barcelo-Ordinas, J. Garcia-Vidal, A. Ripoll and M. Viana, "A Comparative Study of Calibration Methods for Low-Cost Ozone Sensors in IoT Platforms," in IEEE Internet of Things Journal, vol. 6, no. 6, pp. 9563-9571, Dec. 2019, doi: 10.1109/JIOT.2019.2929594. P. Ferrer-Cid, J. M. Barcelo-Ordinas, J. Garcia-Vidal, A. Ripoll and M. Viana, "Multisensor Data Fusion Calibration in IoT Air Pollution Platforms," in IEEE Internet of Things Journal, vol. 7, no. 4, pp. 3124-3132, April 2020, doi: 10.1109/JIOT.2020.2965283.
- 2021-01-24; lectura master thesis Pau Ferrer. 
- 2021-01-25; preprocesamiento de datos, llevados a forma tabular, limpieza de datos de humedad de suelo, purga outliers.
- 2021-01-26; multiple linear regression ok, knn didn't work, studied cross-validation using gridsearchcv
- 2021-01-27; multiple linear regression ok, knn ok, k-fold cross-validation + gridsearchcv -> OK
- 2021-01-28; UPC meeting, random forest, super vector regression. Clase en UPC dictada por el profesor José María Barceló: Topics on optimization and ML
- 2021-01-29; all sensors were calibrated with mlr and knn. R2 and RMSE was computed for each regression method
- 2021-02-01; SVR fixed, Results presentation in videoconference with José María, Pau y Jorge. To do: study state of art, evaluate differents test sizes, evaluate influence of air temperature and humidity
- 2021-02-02; code was improved, binder added 
- 2021-02-03; graphs improvements, run with differents test size, TO DO: fix regressions functions parameters xtrain, ytrain... should be X,y, test_size
- 2021-02-04; videoconferencia con José, Pau, Aina y Andrés. 
- 2021-02-05; search for papers, state of the art revision, clasification of papers
- 2021-02-06; create a python library for regressions, a python wrapper for sklearn called pyregression: https://gitlab.com/pyregression/pyregression.gitlab.io
- 2021-02-08; reading and clasification of papers about calibration of soil moisture sensors: https://gitlab.com/emilopez/lml/-/tree/master/doc/papers/soil-moisture-calibration. improving pyregression
- 2021-02-09; pyregression builded to be installable via pip. First versions using https://test.pypi.org/project/pyregression-emilopez/
- 2021-02-10; pip installable package of pyregression: version 0.0.8 https://pypi.org/project/pyregression/
- 2021-02-11; UPC meeting. Captors node, building process, components and electronics. Exlained by Aina.
- 2021-02-12; Studying chapter 1 and 3 (Linear model regression) from the book Pattern Recognition and Machine Learning,  Christopher M. Bishop.
- 2021-02-13; Studying chapter 1 and 3 (Linear model regression) from the book Pattern Recognition and Machine Learning,  Christopher M. Bishop.
- 2021-02-14; Studying chapter 2.1, 2.2 and 3 from the book An Introduction to Statistical Learning
- 2021-02-15; Possible outlets to the paper was analized: water, sensors, Agricultural Water Management (Q1)
- 2021-02-16; Improve plots: plot 3d scatter plot with soil temp, capacticance and volumetric water content
- 2021-02-17; UPC meeting: building captors in the new box, about the better journal to my paper 
- 2021-02-18; making slides with a summary of the different algorithms
- 2021-02-19; 12..14hs TOML course at UPC (José María). Aula A5-204. 
- 2021-02-21; reading Convex Optimization (S. Boyd). Ch. 1 & 2: least squares, linear programming, affine and convex sets
- 2021-02-22; hyperplane added to 3d scatter plot: https://ml-regression.gitlab.io/scatter3d.html. 16hs - Meeting at UPC with Aina & Andrés.
- 2021-02-23; plot hyperplanes for each low cost sensor with MLR. Adjusted R2 added. Reading Ridge and Lasso regression. 12hs TOML course.
- 2021-02-24; Reading *Feature selection* from [sklearn documentation](https://scikit-learn.org/stable/modules/feature_selection.html). Regressions with dataset #2, only with sensor and soilt temp features. Generating hyperplanes for differents regressions methods.
- 2021-02-25; studying arduino firmware of captor nodes, create a repo and a tagging of the release v1.0.0 with original firmware (no low power). Regressions with dataset #3, very bad results, there are a problem with sensors.
- 2021-02-26; video meeting with Jorge, Pau and José María, comments about results of different datasets. TOML class at UPC.
- 2021-03-1;  power consumption analisys of arduino and raspberry pi at UPC with Aina and Andres. Low power techniques by software
- 2021-03-02; study TOML, and TOML course (virtual).
- 2021-03-03; UPC meeting, showing the results of power consumption, Jorge, Pau, José María and Aina. Defining duty cycle, calcs about autonomy, adding a 3rd arduino to get temp and HR.
- 2021-03-04; working with Aina at UPC: modifying/improving the arduino firmware to work with low power techniques. Not enough memory space, working on it...
- 2021-03-05; TOML course (at UPC)
- 2021-03-08; UPC meeting with Andres and Aina. Study of the complete software system of captor-4.
- 2021-03-09; TOML online course.
- 2021-03-10; UPC Meeting: Zhe explained the general operation of the system. Emiliano described how the arduino firmware works, Aina described the python software and config files of the Raspberry Pi and the AWS workflow.
- 2021-03-11; UPC meeting with Aina y Zhe. Refactoring the Arduino firmware. 
- 2021-03-12; TOML course at UPC.
- 2021-03-13; Refactoring 80% of the Arduino firmware. No more Strings! packet of 20 bytes with timestamp!
- 2021-03-15; UPC meeting with Aina y Zhe: test nuew arduino firmware with sensors and sneding packets via I2C to RPi
- 2021-03-16; Doing the programming optimization exercices. TOML online course. Modifying the arduino firmware to low power. 
- 2021-03-17; Programming from scratch python scrtipt (Raspberry Pi) to comunicate with arduino. Thinking in a kind of 3-way handshake like 802.3. UPC meeting, explain the new firmware and software.
- 2021-03-18; Refactoring the arduino firmware according to the new requirements. 
- 2021-03-19; Still working with the Arduino firmware. TOML course at UPC. 
- 2021-03-20; Report writing. 
- 2021-03-22; Working with low power arduino firmware
- 2021-03-23; Developing python script from raspberry pi to talk with low power aduino firmware
- 2021-03-24; UPC meeting, talk about low power technique and realted problems. Testing sleepy pi with Zhe Ye. 
- 2021-03-25; UPC meeting with Zhe Ye, working with TPL5110, testing raspberry pi.
- 2021-03-26; TOML course at UPC.
